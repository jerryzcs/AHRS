#include <acc_comp.h>
#include <math.h>
#include <Matrix.h>


float ax, ay, az; //观测量数据
float w_x, w_y, w_z;
float seq[4];//K(Z-Z~)，卡尔曼的反馈量
float w[3];
float a[3];
float q[4];
float q_back[4];
float qkk[4];//当前四元数
float qk[4];
float af[3];


extern float PP[16];
extern int yuce_only;
float R[3]={1,0.5,0.5};//观测量的方差阵，这个地方可以取的大一点
void acc_filterUpdate_UD(float* wk,float* qkk,float* a, float* seq)
{ 


int i=0;
float PP2[16];//状态量的协方差矩阵估计值
float J[12];//观测状态方程矩阵
float K[12];//卡尔曼增益矩阵
float KJ[16];
float PinvJ[12];
float pz2[16];//状态变量的协方差预测
float pz[16];
float sita[16];//状态方程矩阵
float invsita[16];
float seqd[12];
float invJ[12];
float seqd2[9];/*inv(HP(k,k-1)HT+R)，H代表J矩阵，P（k,k-1）为状态变量的协方差预测，HT为H矩阵的转置矩阵，R为观察量的方差矩阵*/
float U[16];//状态量协方差矩阵预测阵UD分解出来的矩阵
float D[16];//同上
float seqd5[12];
float UT[16];//U矩阵的转置矩阵
float seqd6[16];
float seqd7[12];
float FW[12];
float seqd7T[12];
float FWFT[16];
float DSubFWFT[16];
float UDSubFWFT[16];

float q0;
float q1;
float q2;
float q3;
float dcm00 ;
float dcm01 ;
float dcm02 ;
float dcm12 ;
float dcm22 ;
float phi_err ;
float theta_err ;
float psi_err ;
float ax;
float ay;
float az;
float a1;
ax=a[0];
ay=a[1];
az=a[2];

q0=qkk[0];
q1=qkk[1];
q2=qkk[2];
q3=qkk[3];

 dcm00 = 1.0-2*(q2*q2 + q3*q3);
 dcm01 =     2*(q1*q2 + q0*q3);
 dcm02 =     2*(q1*q3 - q0*q2);
 dcm12 =     2*(q2*q3 + q0*q1);
 dcm22 = 1.0-2*(q1*q1 + q2*q2);
 phi_err = 2. / (dcm22*dcm22 + dcm12*dcm12);
 theta_err = -2. / sqrt( 1 - dcm02*dcm02 );
 psi_err = 2. / (dcm00*dcm00 + dcm01*dcm01);
 /*AHRS四元数是+-180,需要转换一次*/
 if(atan2(2*(q1*q2+q0*q3),q0*q0+q1*q1-q2*q2-q3*q3)<0)
 {
 a1=atan2(2*(q1*q2+q0*q3),q0*q0+q1*q1-q2*q2-q3*q3)+2*3.1415926;
 }
 else
 {
 a1=atan2(2*(q1*q2+q0*q3),q0*q0+q1*q1-q2*q2-q3*q3);
 }
 /*af=Z-Z~，观测量与观测量预测值之差*/

af[0]=ax-a1;
if(af[0]>3.1415926||af[0]<-3.1415926)
{
af[0]=0;
}
else
{
af[0]=af[0];
}
af[1]=ay-asin(-2*(q1*q3-q0*q2));
af[2]=az-atan2(2*(q2*q3+q0*q1),q0*q0-q1*q1-q2*q2+q3*q3);
/*sita为状态矩阵*/
sita[0]=wk[0];
sita[1]=wk[1];
sita[2]=wk[2];
sita[3]=wk[3];

sita[4]=wk[4];
sita[5]=wk[5];
sita[6]=wk[6];
sita[7]=wk[7];

sita[8]=wk[8];
sita[9]=wk[9];
sita[10]=wk[10];
sita[11]=wk[11];

sita[12]=wk[12];
sita[13]=wk[13];
sita[14]=wk[14];
sita[15]=wk[15];
/*J为经过变化后的观测矩阵*/
J[0]= (q3 * dcm00) * psi_err;
J[1]= (q2 * dcm00) * psi_err;
J[2]= (q1 * dcm00 + 2 * q2 * dcm01) * psi_err;
J[3]= (q0 * dcm00 + 2 * q3 * dcm01) * psi_err;

J[4]= -q2 * theta_err;
J[5]=  q3 * theta_err;
J[6]= -q0 * theta_err;
J[7]=  q1 * theta_err;

J[8]= (q1 * dcm22) * phi_err;
J[9]= (q0 * dcm22 + 2 * q1 * dcm12) * phi_err;
J[10]= (q3 * dcm22 + 2 * q2 * dcm12) * phi_err;
J[11]= (q2 * dcm22) * phi_err;




MatrixTranspose(sita,4,4,invsita);
MatrixMultiply(sita,4,4,PP,4,4,pz);
MatrixMultiply(pz,4,4,invsita,4,4,pz2);//P（k,k-1）=sita*PP*sitaT 

pz2[0]=pz2[0]+0.001;
pz2[5]=pz2[5]+0.001;
pz2[10]=pz2[10]+0.001;
pz2[15]=pz2[15]+0.001;//+Q


UD(pz2,4,U,D);/*对P阵进行UD分解*/


MatrixTranspose(J,3,4,invJ);//求转置矩阵
MatrixTranspose(U,4,4,UT);//求转置矩阵
MatrixMultiply(D,4,4,UT,4,4,seqd6);
MatrixMultiply(seqd6,4,4,invJ,4,3,seqd7);///F=seq7
MatrixMultiply(U,4,4,seqd7,4,3,seqd5);///G=U*F
MatrixMultiply(J,3,4,seqd5,4,3,seqd2);//HPH'=seqd2
//HPH'+R

seqd2[0]=seqd2[0]+R[0];
seqd2[1]=seqd2[1];                     
seqd2[2]=seqd2[2];

seqd2[3]=seqd2[3];
seqd2[4]=seqd2[4]+R[1];
seqd2[5]=seqd2[5];

seqd2[6]=seqd2[6];
seqd2[7]=seqd2[7];
seqd2[8]=seqd2[8]+R[2];
//(HPH'+R)^-1,PK[9]
MatrixInverse(seqd2,3,0);


MatrixMultiply(seqd5,4,3,seqd2,3,3,K);//K=G*W^-1增益阵


MatrixMultiply(seqd7,4,3,seqd2,3,3,FW);
MatrixTranspose(seqd7,4,3,seqd7T);
MatrixMultiply(FW,4,3,seqd7T,3,4,FWFT);

MatrixSub(D,FWFT,DSubFWFT,4,4);
MatrixMultiply(U,4,4,DSubFWFT,4,4,UDSubFWFT);
MatrixMultiply(UDSubFWFT,4,4,UT,4,4,PP2);
MatrixMultiply(K,4,3,af,3,1,seq);//seq[7],滤波结果
if(yuce_only==1)
{
for(i=0;i<4;i++)
{
seq[i]=0;
}
}
else
{


}
for(i=0;i<16;i++)
{
PP[i]=PP2[i];
}
}




